/**
 * Created with JetBrains PhpStorm.
 * User: oyewale
 * Date: 1/23/14
 * Time: 10:44 AM
 * To change this template use File | Settings | File Templates.
 */
var currentReceiverId = 0;

$(document).ready(function(){
//    $(function() {
//        var cache = {};
//        $( "#birds" ).autocomplete({
//            minLength: 2,
//            source: function( request, response ) {
//                var term = request.term;
//                if ( term in cache ) {
//                    response( cache[ term ] );
//                    return;
//                }
//
//                $.getJSON( "search.php", request, function( data, status, xhr ) {
//                    cache[ term ] = data;
//                    response( data );
//                });
//            }
//        });
//    });
    $( ".user:first").trigger( "click" );

    $("#reply-text").keydown(function(e){
        if(e.keyCode == 13){
            postMessage();
        }
    });

//    $('.user').click(function(){
//        var uid =$('this .uid').val();
//        alert(uid);
//    });
});

function postMessage(){
    var msg = $('#reply-text').val();
//    alert(msg);
    if (msg.length < 1){
        return;
    }
    sendMessage(msg);
}
var message = '';
function sendMessage(msg){
    message = msg;
    $.ajax({
        type: "POST",
        url: 'api/sendmessage.php',
        data: {receiver_id: currentReceiverId, message: msg },
        success: messageSentHandler,
        dataType: 'json'
    });
}

function messageSentHandler(e){
    console.log(e);
//    alert("Message Response Recieved");
    if (e.success){
        var html =
            '<div class="media msg">'+
                '<a class="pull-left" href="#">'+
                '<img class="media-object" data-src="holder.js/64x64" alt="64x64" style="width: 32px; height: 32px;"' +
                ' src="images/clarity.png">' +
                '</a>'+
                '<div class="media-body">'+
                '<small class="pull-right time"><i class="fa fa-clock-o"></i> 12:10am</small>'+
                '<h5 class="media-heading">Me:</h5>'+
                '<small class="col-lg-10">'+ message +'</small>'+
                '</div>'+
                '</div>';
        $('#msg-holder').append(html);
    } else {
        alert("Error Posting Message");
    }
    $('#reply-text').val('');
}

function getMessageFromUser(uid){
//    alert(uid);
    currentReceiverId = uid;
    $.ajax({
        type: "POST",
        url: 'api/receivemessage.php',
        data: {receiver_id: uid },
        success: renderMsg,
        dataType: 'json'
    });
}

function renderMsg(e){
    //alert(e);
    console.log(e);
    if (!e.success){
        return;
    }
    var msgs = e.messages;
    var html = '';
    for (var i=0; i<msgs.length; i++){
        var msg = msgs[i];
        var name = (parseInt(msg.sender_id) === uid)? 'Me':msg.username;
        html +=
            '<div class="media msg">'+
                '<a class="pull-left" href="#">'+
                '<img class="media-object" data-src="holder.js/64x64" alt="64x64" style="width: 32px; height: 32px;"' +
                ' src="images/clarity.png">' +
                '</a>'+
                '<div class="media-body">'+
                    '<small class="pull-right time"><i class="fa fa-clock-o"></i> 12:10am</small>'+
                       '<h5 class="media-heading">'+ name +':</h5>'+
                    '<small class="col-lg-10">'+ msg.message +'</small>'+
                '</div>'+
            '</div>';



    }
    $('#msg-holder').html(html);

}

function redirectToLogin(){
    document.location.href = './';
}

//function success(val){
//    alert(val.messages);
//}