/**
 * Created with JetBrains PhpStorm.
 * User: oyewale
 * Date: 1/23/14
 * Time: 11:07 AM
 * To change this template use File | Settings | File Templates.
 */

$(document).ready(function(){
    $( "#register-form" ).submit(function( event ) {
        //alert( "Handler for .submit() called." );
        event.preventDefault();
        var username = $('#username').val();
        var password = $('#password').val();
        var firstname = $('#firstname').val();
        var lastname =  $('#lastname').val();

        if (password != $('#password_confirmation').val()){
            return;
        }

        $.ajax({
            type: "POST",
            url: 'api/register.php',
            data: {
                username: username,
                password: password,
                firstname: firstname,
                lastname: lastname
            },
            success: success,
            dataType: 'json'
        });
    });

});

function success(e){
//    alert(e);
    console.log(e);
    if (e.success){
        document.location.href = './dashboard.php'
    } else {
        alert ('Username already taken!! Please select another username.')
    }
}

$(function(){
    $('.button-checkbox').each(function(){
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });

        $checkbox.on('change', function () {
            updateDisplay();
        });

        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');
            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else
            {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }
        function init() {
            updateDisplay();
            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });



});
