/**
 * Created with JetBrains PhpStorm.
 * User: oyewale
 * Date: 1/23/14
 * Time: 10:38 AM
 * To change this template use File | Settings | File Templates.
 */

$(document).ready(function(){
    $( "#login-form" ).submit(function( event ) {
        //alert( "Handler for .submit() called." );
        event.preventDefault();


        $.ajax({
            type: "POST",
            url: 'api/login.php',
            data: {username: $('#username').val(), password: $('#password').val() },
            success: success,
            dataType: 'json'
        });
    });

});

function success(e){
//    alert(e);
    console.log(e);
    if (e.success){
        document.location.href = './dashboard.php'
    }
}




$(function(){
    $('.button-checkbox').each(function(){
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });

        $checkbox.on('change', function () {
            updateDisplay();
        });

        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');
            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else
            {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }
        function init() {
            updateDisplay();
            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
});
