<?php
/**************************************************
*	code written by: Ibrahim Abiola Ibrahim
*	Date: 29th May, 2009.
*	For the Student Record Manager Solution UI.
***************************************************/
class DbConn {
    private $dbQuery;
    private $dbServer;
    private $dbName;
    private $username;
    private $password;
    private $dbArray;
    /**
     * Constructor
     * @param <type> $server database server e.g. localhost
     * @param <type> $database database to connect to
     * @param <type> $username database username
     * @param <type> $password database password
     */
    function __construct($server, $database, $username, $password){
        $this->dbServer = $server;
        $this->dbName = $database;
        $this->username = $username;
        $this->password = $password; 
    }
    /**
     * function for database connection
     * @return true if successful
     */
    function connect(){
        mysql_connect($this->dbServer, $this->username, $this->password);
        mysql_select_db($this->dbName);
        return true;
    }
    /**
     * function to query the database
     * @param <type> $query the string containing the sql query
     * @return <type> two dimensional array representing the resultset
     */
    function executeQuery($query){
        $this->dbQuery = $query;
        $this->connect();
        $this->dbArray = mysql_query($this->dbQuery);
        $i = 0;
        $retArray = array();
        while($row = mysql_fetch_array($this->dbArray, MYSQL_BOTH)){
            $retArray[$i] = $row;
            $i++;
        }
        return $retArray;
    }
    /**
     * function to update the database
     * escape Strings coming from textarea
     * @param <type> $query the string containing the sql code
     * @return <type> number of rows affected
     */
    function executeUpdate($query){
        //$query = mysql_real_escape_string($query);
        $this->dbQuery = $query;
        $this->connect();
        if(mysql_query($query)){
            return mysql_affected_rows();
        }else{
            return 0;
        }
    }
    /**
     * function to retrieve the last database query
     * @return <type> the last resultset
     */
    function lastQuery(){
        return $this->dbQuery;
    }
}
?>
