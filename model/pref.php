<?php
class Settings{
    private $host;
    private $database;
    private $username;
    private $password;

    function __construct(){
        $server = $_SERVER['HTTP_HOST'];
        if($server == 'localhost' || $server == '127.0.0.1'){
            $this->host = 'localhost';
            $this->database = 'chatapplication';
            $this->username = 'root';
            $this->password = 'root';
        }else{
            $this->host = 'localhost';
            $this->database = '';
            $this->username = '';
            $this->password = '';
        }
    }

    function get_host(){
        return $this->host;
    }

    function get_database(){
        return $this->database;
    }

    function get_username(){
        return $this->username;
    }

    function get_password(){
        return $this->password;
    }

}
?>