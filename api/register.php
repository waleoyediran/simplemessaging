<?php

include '../model/data.php';

session_start();
$data = new Data();

$username = $_POST['username'];
$password = $_POST['password'];
$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];

$resp = array("success" => false);
if ($data->createUser($username, $password, $firstname, $lastname)) {
    session_start();
    $id = mysql_insert_id();
    $_SESSION["user"] = array("name" => $username, 'id' => $id);
    $resp = array('success' => true, 'id'=>$id);
}

echo json_encode($resp);
