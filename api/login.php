<?php

include '../model/data.php';

$data = new Data();

$username = $_POST['username'];
$password = $_POST['password'];

$resp = array("success" => false);
$id = $data->login($username, $password);
if ($id) {
    $resp = array('success' => true);
    session_start();
    $_SESSION["user"] = array("name" => $username, 'id' => $id);
}

echo json_encode($resp);
