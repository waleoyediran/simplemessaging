<?php

include '../model/data.php';

session_start();
$data = new Data();

$receiver = $_POST['receiver_id'];
$message = $_POST['message'];

$user = $_SESSION['user'];

$resp = array("success"=>false, 'msg'=>'No User');

if (!$user){
    echo json_encode($resp);
    return;
}

if ($data->sendMessage($user['id'], $receiver, $message)){
    $resp = array("success"=> true);
    echo json_encode($resp);
    return;
}

$resp = array("success"=>false, 'msg'=>'Message Not sent');
echo json_encode($resp);
return;
