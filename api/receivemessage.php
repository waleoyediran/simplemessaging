<?php

include '../model/data.php';

session_start();

// Initialize application Class
$data = new Data();

// Get Messages From A User
$receiver = $_POST['receiver_id'];

// Currently Logged in User
$user = $_SESSION['user'];

// Default Return Object for Not Logged in User
$resp = array("success"=>false, 'message'=>'No User');

if (!$user){
    echo json_encode($resp);
    return;
}

$messages = $data->getMessages($user["id"], $receiver, 0);
$resp = array("success"=>true, "messages" => $messages);

echo json_encode($resp);