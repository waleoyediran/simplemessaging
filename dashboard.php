<?php
    error_reporting(E_ALL);

    session_start();
    $user = $_SESSION['user'];
    if (!$user){
        header('location: ./');
    }


    include('model/data.php');

    // Load Application Class
    $data = new Data();

    // Get Other Application Users
    $users = $data->getUsers($user['id']);


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>QwikMessage</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS for the 'Full Width Pics' Template -->
<!--    <link href="css/full-width-pics.css" rel="stylesheet">-->
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link href="css/convo.css" rel="stylesheet">
    <script>
        var uid = <?php echo $user['id']?>;
    </script>
</head>

<body>

<nav class="navbar navbar-fixed-top navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">QwikMessage</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li><a href="#about">About</a></li>
            </ul>
            <ul class="nav navbar-nav pull-right">
                <li class="dropdown">
                    <a href="#"
                       class="dropdown-toggle"
                       data-toggle="dropdown">
                        <?php echo $user["name"]?>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="socials"><a href="./api/logout.php" class="" >Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->

    </div><!-- /.container -->
</nav>

<div class="container" style="padding-top: 60px;">
    <div class="row">
        <div class="col-lg-3">
            <div class="btn-panel btn-panel-conversation">
                <a href="" class="btn  col-lg-6 send-message-btn " role="button"><i class="fa fa-search"></i> Search</a>
                <a href="" class="btn  col-lg-6  send-message-btn pull-right" role="button"><i class="fa fa-plus"></i> New Message</a>
            </div>
        </div>

        <div class="col-lg-offset-1 col-lg-7">
            <div class="btn-panel btn-panel-msg">

                <a href="" class="btn  col-lg-3  send-message-btn pull-right" role="button"><i class="fa fa-gears"></i> Settings</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="conversation-wrap col-lg-3">
            <div class="media conversation">
                <div class="media-body">
                    <div class="ui-widget">
                        <input id="search">
                    </div>
                </div>
            </div>

            <?php foreach($users as $user):?>

            <div class="media conversation user" onclick="getMessageFromUser(<?php echo $user['id'] ?>)">
                <a class="pull-left" href="#">
                    <img class="media-object" data-src="holder.js/64x64" alt="64x64" style="width: 50px; height: 50px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAACqUlEQVR4Xu2Y60tiURTFl48STFJMwkQjUTDtixq+Av93P6iBJFTgg1JL8QWBGT4QfDX7gDIyNE3nEBO6D0Rh9+5z9rprr19dTa/XW2KHl4YFYAfwCHAG7HAGgkOQKcAUYAowBZgCO6wAY5AxyBhkDDIGdxgC/M8QY5AxyBhkDDIGGYM7rIAyBgeDAYrFIkajEYxGIwKBAA4PDzckpd+322243W54PJ5P5f6Omh9tqiTAfD5HNpuFVqvFyckJms0m9vf3EY/H1/u9vb0hn89jsVj8kwDfUfNviisJ8PLygru7O4TDYVgsFtDh9Xo9NBrNes9cLgeTybThgKenJ1SrVXGf1WoVDup2u4jFYhiPx1I1P7XVBxcoCVCr1UBfTqcTrVYLe3t7OD8/x/HxsdiOPqNGo9Eo0un02gHkBhJmuVzC7/fj5uYGXq8XZ2dnop5Mzf8iwMPDAxqNBmw2GxwOBx4fHzGdTpFMJkVzNB7UGAmSSqU2RoDmnETQ6XQiOyKRiHCOSk0ZEZQcUKlU8Pz8LA5vNptRr9eFCJQBFHq//szG5eWlGA1ywOnpqQhBapoWPfl+vw+fzweXyyU+U635VRGUBOh0OigUCggGg8IFK/teXV3h/v4ew+Hwj/OQU4gUq/w4ODgQrkkkEmKEVGp+tXm6XkkAOngmk4HBYBAjQA6gEKRmyOL05GnR99vbW9jtdjEGdP319bUIR8oA+pnG5OLiQoghU5OElFlKAtCGr6+vKJfLmEwm64aosd/XbDbbyIBSqSSeNKU+HXzlnFAohKOjI6maMs0rO0B20590n7IDflIzMmdhAfiNEL8R4jdC/EZIJj235R6mAFOAKcAUYApsS6LL9MEUYAowBZgCTAGZ9NyWe5gCTAGmAFOAKbAtiS7TB1Ng1ynwDkxRe58vH3FfAAAAAElFTkSuQmCC">
                </a>
                <div class="media-body">
                    <input type="hidden" class="uid" value="<?php echo $user['id']?>"/>
                    <h5 class="media-heading"><?php echo $user['username']?></h5>
                    <small>Hello</small>
                </div>
            </div>

            <?php endforeach ?>

        </div>



        <div class="message-wrap col-lg-8">
            <div class="msg-wrap" id="msg-holder">


<!--                <div class="media msg ">-->
<!--                    <a class="pull-left" href="#">-->
<!--                        <img class="media-object" data-src="holder.js/64x64" alt="64x64" style="width: 32px; height: 32px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAACqUlEQVR4Xu2Y60tiURTFl48STFJMwkQjUTDtixq+Av93P6iBJFTgg1JL8QWBGT4QfDX7gDIyNE3nEBO6D0Rh9+5z9rprr19dTa/XW2KHl4YFYAfwCHAG7HAGgkOQKcAUYAowBZgCO6wAY5AxyBhkDDIGdxgC/M8QY5AxyBhkDDIGGYM7rIAyBgeDAYrFIkajEYxGIwKBAA4PDzckpd+322243W54PJ5P5f6Omh9tqiTAfD5HNpuFVqvFyckJms0m9vf3EY/H1/u9vb0hn89jsVj8kwDfUfNviisJ8PLygru7O4TDYVgsFtDh9Xo9NBrNes9cLgeTybThgKenJ1SrVXGf1WoVDup2u4jFYhiPx1I1P7XVBxcoCVCr1UBfTqcTrVYLe3t7OD8/x/HxsdiOPqNGo9Eo0un02gHkBhJmuVzC7/fj5uYGXq8XZ2dnop5Mzf8iwMPDAxqNBmw2GxwOBx4fHzGdTpFMJkVzNB7UGAmSSqU2RoDmnETQ6XQiOyKRiHCOSk0ZEZQcUKlU8Pz8LA5vNptRr9eFCJQBFHq//szG5eWlGA1ywOnpqQhBapoWPfl+vw+fzweXyyU+U635VRGUBOh0OigUCggGg8IFK/teXV3h/v4ew+Hwj/OQU4gUq/w4ODgQrkkkEmKEVGp+tXm6XkkAOngmk4HBYBAjQA6gEKRmyOL05GnR99vbW9jtdjEGdP319bUIR8oA+pnG5OLiQoghU5OElFlKAtCGr6+vKJfLmEwm64aosd/XbDbbyIBSqSSeNKU+HXzlnFAohKOjI6maMs0rO0B20590n7IDflIzMmdhAfiNEL8R4jdC/EZIJj235R6mAFOAKcAUYApsS6LL9MEUYAowBZgCTAGZ9NyWe5gCTAGmAFOAKbAtiS7TB1Ng1ynwDkxRe58vH3FfAAAAAElFTkSuQmCC">-->
<!--                    </a>-->
<!--                    <div class="media-body">-->
<!--                        <small class="pull-right time"><i class="fa fa-clock-o"></i> 12:10am</small>-->
<!--                        <h5 class="media-heading">Naimish Sakhpara</h5>-->
<!--                        <small class="col-lg-10">Location H-2, Ayojan Nagar, Near Gate-3, Near-->
<!--                            Shreyas Crossing Dharnidhar Derasar,-->
<!--                            Paldi, Ahmedabad 380007, Ahmedabad,-->
<!--                            India-->
<!--                            Phone 091 37 669307-->
<!--                            Email aapamdavad.district@gmail.com</small>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="alert alert-info msg-date">-->
<!--                    <strong>Today</strong>-->
<!--                </div>-->
<!--                <div class="media msg">-->
<!--                    <a class="pull-left" href="#">-->
<!--                        <img class="media-object" data-src="holder.js/64x64" alt="64x64" style="width: 32px; height: 32px;" src="images/clarity.png">-->
<!--                    </a>-->
<!--                    <div class="media-body">-->
<!--                        <small class="pull-right time"><i class="fa fa-clock-o"></i> 12:10am</small>-->
<!---->
<!--                        <h5 class="media-heading">Whales</h5>-->
<!--                        <small class="col-lg-10">Testing</small>-->
<!--                    </div>-->
<!--                </div>-->

            </div>

            <div class="send-wrap ">

                <input class="form-control send-message"  placeholder="Write a reply..." id="reply-text" />


            </div>
            <div class="btn-panel">
                <a href="" class=" col-lg-3 btn   send-message-btn " role="button"><i class="fa fa-cloud-upload"></i> Add Files</a>
                <a href="" class=" col-lg-4 text-right btn   send-message-btn pull-right" role="button"><i class="fa fa-plus"></i> Send Message</a>
            </div>
        </div>
    </div>
</div>



                <!-- FOOTER -->
<div class="container">
    <footer>
        <div class="row">
            <div class="col-lg-12">
                Copyright &copy; QwikMessaging 2014
            </div>
        </div>
    </footer>
</div>

</div><!-- /container -->

<!-- JavaScript -->
<script src="js/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/dashboard.js"></script>

</body>

</html>


<?php
/**
 * Created by JetBrains PhpStorm.
 * User: oyewale
 * Date: 1/23/14
 * Time: 10:35 AM
 * To change this template use File | Settings | File Templates.
 */

